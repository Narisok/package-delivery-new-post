<?php

namespace Narisok\DeliveryNewPost;

use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Narisok\DeliveryMethods\DeliveryInterface;
use Narisok\DeliveryMethods\Models\DeliveryCity;
use Narisok\DeliveryMethods\Models\DeliveryWarehouse;

class NewPostService implements DeliveryInterface
{
    
    public function getName()
    {
        return 'NewPost';
    }

    public function update()
    {
        $status = $this->updateCities();
        if($status) {
            $status = $this->updateWarehouses();
        }
        return [
            'status' => $status,
        ];
    }

    public function updateWarehouses()
    {
        $update = true;
        $currentPage = 1;
        $status = true;
        while($update) {
            $data = json_decode('{
                "modelName": "Address",
                "calledMethod": "getWarehouses",
                "methodProperties": {
                    "Page": '.$currentPage.',
                    "Limit": 200
                }
            }');
            $response = Http::post('https://api.novaposhta.ua/v2.0/json/', [$data]);
            $resultData = [];
            $responseData =  $response->json();
            if (array_key_exists(0, $responseData)) {
                $responseData = $responseData[0];
            }
            if($response->getStatusCode() == 200) {
                $update = count($responseData['data']) > 0;
                foreach($responseData['data'] as $warehouse) {
                    try{
                        $model = DeliveryWarehouse::updateOrCreate([
                            'method' => $this->getName(),
                            'ref' => $warehouse['Ref'],
                        ], [
                            'delivery_city_ref' => $warehouse['CityRef'],
                            'number' => $warehouse['Number'],
                        ]);

                        $model->translations()->updateOrCreate([
                            'locale' => 'uk',
                        ], [
                            'title' => substr($warehouse['Description'],0,190),
                        ]);
                        $model->translations()->updateOrCreate([
                            'locale' => 'ru',
                        ], [
                            'title' => substr($warehouse['DescriptionRu'],0,190),
                        ]);

                    } catch(Exception $e) {
                        Log::warning($e->getMessage());
                    }
                }
            } else {
                $update = false;
                $status = false;
            }
            $currentPage++;
        }

        return [
            'status' => $status,
        ];
    }

    public function updateCities()
    {
        $currentPage = 1;
        $update = true;
        $status = true;
        while($update) {
            $data = json_decode('{
                "modelName": "Address",
                "calledMethod": "getCities",
                "methodProperties": {
                    "Page": '.$currentPage.',
                    "Limit": 500
                }
            }');
            $response = Http::post('https://api.novaposhta.ua/v2.0/json/', [$data]);
            $resultData = [];
            if($response->getStatusCode() == 200) {
                $update = count($response->json()['data']) > 0;
                foreach($response->json()['data'] as $city) {
                    $model = DeliveryCity::updateOrCreate([
                        'method' => $this->getName(),
                        'ref' => $city['Ref']
                    ],[]);
                    $model->translations()->updateOrCreate([
                        'locale' => 'uk',
                    ], [
                        'title' => $city['Description']
                    ]);
                    $model->translations()->updateOrCreate([
                        'locale' => 'ru',
                    ], [
                        'title' => $city['DescriptionRu']
                    ]);
                }
            } else {
                $update = false;
                $status = false;
            }
            $currentPage++;
        }

        return [
            'status' => $status,
        ];
    }
}

