<?php

namespace Narisok\DeliveryNewPost;

use Illuminate\Support\ServiceProvider;
use Narisok\DeliveryMethods\DeliveryService;

class DeliveryNewPostProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        resolve(DeliveryService::class)->registerMethod(new NewPostService());
    }
}
